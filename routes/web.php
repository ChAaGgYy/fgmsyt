<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/back', function () {
    return back();
});

// Route::get('/prueba', function () {
//     return view('proyectos.Nuevo_Proyecto');
// });
// Auth::routes();
// Route::post('Nuevo_Productor', 'ProductorController@store');

Route::resource('Proyecto', 'ProyectosController');
Route::resource('Productor', 'ProductorController');
Route::get('/Lista_Productores', 'ProductorController@consulta');
Route::get('/Lista_Proyectos', 'ProyectosController@consulta');


Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/municipios/{id}', 'ProyectosController@getmunicipios');