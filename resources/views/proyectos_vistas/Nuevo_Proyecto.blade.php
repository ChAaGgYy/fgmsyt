@extends('layouts.app')

@section('title', 'Nuevo Proyecto')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-3"><a class="btn btn-primary" href="/home">Salir</a></div>
                        <div class="col-md-6"><h1>Registro de Proyectos</h1></div>
                    </div>
                </div>
                <div class="card-body">
                    {!! Form::open(['url' => 'Proyecto'], ['method' => 'POST']) !!}
                        {{-- {!! Form::open(['action' => 'Controller@store']) !!} --}}
                        {{-- {!! Form::open(['route' => 'Nuevo_Proyecto']) !!} --}}
                            {{-- {!! Form::token() !!} --}}
                        <div class="row">
                            <div class="form-group col-md-2">
                                {!! Form::label('numero_pry', 'N°:' ) !!}
                                {!! Form::text('numero_pry', null, ['class' => 'form-control' ]) !!}
                                {{-- @if($errors->has('numero_pry'))
                                    <span style="color: red">{!! $errors->first('numero_pry') !!}</span>
                                @endif --}}
                            </div>
                            <div class="form-group col-md-10">
                                {!! Form::label('nombre_pry', 'Nombre del Proyecto' ) !!}
                                {!! Form::text('nombre_pry', null, ['class' => 'form-control' ]) !!}
                                {{-- @if($errors->has('nombre_pry'))
                                    <span style="color: red">{!! $errors->first('nombre_pry') !!}</span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                {!! Form::label('descrip_pry', 'Descripción del Proyecto' ) !!}
                                {!! Form::textarea('descrip_pry', null,['class' => 'form-control', 'rows' => 3]) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                {!! Form::label('act_econom_pry', 'Actividad Economica' ) !!}
                                {!! Form::text('act_econom_pry', null, ['class' => 'form-control' ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('registrado', 'Esta Registrada?' ) !!}  {{-- .- Esta Registrada (booleano) --}}
                                {!! Form::select('registrado', ['t' => 'SI', 'f' => 'NO'], null, ['class' => 'form-control']) !!}
                            </div>
                        {{-- </div>
                        <div class="row"> --}}
                            <div class="form-group col-md-1">
                                {!! Form::label('id_letra_rif', 'RIF' ) !!}
                                {!! Form::select('id_letra_rif', $letras->pluck('letra', 'id'), null, [{{-- 'placeholder' => 'Tipo de Rif', --}} 'class' => 'form-control']); !!}
                            </div>
                            <div class="form-group col-md-2">
                                {!! Form::label('guion1', '-' ) !!}
                                {!! Form::text('numer_rif', null, ['placeholder' => 'Número del Rif', 'class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-md-1">
                                {!! Form::label('guion2', '-' ) !!}
                                {!! Form::select('numer_ident_rif_pry', ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '0' => '0'], null, ['placeholder' => 'Número de Chequeo', 'class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('fech_ini', 'Fecha de Inicio' ) !!}  {{-- .- Esta Registrada (booleano) --}}
                                {{-- {!! Form::date('fech_ini', null, ['class' => 'form-control']) !!} --}}
                                {!! Form::date('fech_ini', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                {!! Form::label('l_id_estado', 'Estados' ) !!}
                                {!! Form::select('id_estado', $estados->pluck('estado', 'id'), isset($estados) ? $estados : null, ['class' => 'form-control box-size', 'id'=>'id_estado']); !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('l_id_municipio', 'Municipios' ) !!}
                                {!! Form::select('id_municipio', $municipios->pluck('municipio', 'id'), null, ['class' => 'form-control box-size']); !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('l_id_parroquia', 'Parroquias' ) !!}
                                {!! Form::select('id_parroquia', $parroquias->pluck('parroquia', 'id'), null, ['class' => 'form-control box-size']); !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('l_id_ciudad', 'Ciudad' ) !!}
                                {!! Form::select('id_ciudad', $ciudades->pluck('ciudad', 'id'), null , ['class' => 'form-control box-size']); !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                {!! Form::label('codigo_situr', 'Codigo Situr ' ) !!}
                                {!! Form::text('codigo_situr', null, ['class' => 'form-control' ]) !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('codigo_sunagro', 'Codigo Sunagro ' ) !!}
                                {!! Form::text('codigo_sunagro', null, ['class' => 'form-control' ]) !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('telefono', 'Teléfono ' ) !!}
                                {!! Form::text('telefono', null, ['class' => 'form-control' ]) !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('direccion', 'Dirección ' ) !!}
                                {!! Form::text('direccion', null, ['class' => 'form-control' ]) !!}
                            </div>
                            {{-- captura de los errores(campos vacios) --}}
                        </div>
                        @include('layouts.errors')
                        {!! Form::submit('Registrar', ['class' => 'btn btn-primary' ]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>



// $('#id_estado').on('change', function(e){
//    console.log(e);

//    var id_municipio = e.target.value;

//    // ajax
//    $.get('/municipios/' + id_municipio, function(data){
//     console.log(data);
//    });
// });


// el jquery para el cambio seria este:
// $("#id_estado").change(function()
// {

//    if($("#id_estado").val() != '') {
//        var url = "";
//        url = url.replace(":id", $("#id_estado").val());

//        // Consultar el producto
//        $.getJSON(url, function( item ) {
//            select = '<select name="catalogo_detalle_id" class="form-control input-sm " required id="sucursals" >';
//            $.each(item, function( index, value ) {
//                select +='<option value="'+value.id+'">'+value.catalogo_detalle_name+'</option>';
//             });
//             select += '</select>';
//             $("#catalogo_detalle_id").html(select);
//        });
//    } else {

//    }
// });
</script>

{{-- //  El controlador para cargar el ajax

public function getTallaTipo($id)
    {
        return response()->json(CatalogoDetalle::where('catalogo_id', $id)->get());
    }
   --}}