@extends('layouts.app')

@section('title', 'Nuevo Productor')

@section('content')
<div class="container">
     <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-3"><a class="btn btn-primary" href="/home">Salir</a></div>
                        <div class="col-md-6"><h1>Listado de Proyectos</h1></div>
                        <div class="col-md-3"><a href="/Proyecto/create" class="btn btn-primary">Registrar Proyecto</a></div>
                    </div>
                </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                    <thead>
                                        <th class="info"><h4>Numero</h4></th>
                                        <th class="info"><h4>Nombre del Proyecto</h4></th>
                                        <th class="info"><h4>Descripcion</h4></th>
                                        <th class="info"><h4>Accion</h4></th>
                                    </thead>
                                <body>
                                 @foreach ($consulta as $consult)
                                    <tr>
                                        <td>{{ $consult-> numero_pry }}</td>
                                        <td>{{ $consult-> nombre_pry }} {{-- {{ $consult-> capeproductor }} --}}</td>
                                        <td>{{ $consult-> descrip_pry }}</td>
                                        <td>
                                            <a href="/{{ $consult->id }}/edit" class="btn btn-warning btn-sm">Accion</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </body>
                            </table>
                        </div>
                    {{ $consulta->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection