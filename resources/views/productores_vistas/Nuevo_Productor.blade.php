@extends('layouts.app')

@section('title', 'Nuevo Productor')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-3"><a class="btn btn-primary" href="/home">Salir</a></div>
                        <div class="col-md-9"><h2>Registro de Productor</h2></div>
                    </div>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'Productor.store', 'method' => 'POST'] ) !!}
                        <div class="form-group">
                            {!! Form::label('cnombproductor', 'Nombre' ) !!}
                            {!! Form::text('cnombproductor', null, ['class' => 'form-control' ])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('capeproductor', 'Apellido' ) !!}
                            {!! Form::text('capeproductor', null, ['class' => 'form-control' ]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ncedula', 'Cedula' ) !!}
                            {!! Form::text('ncedula', null, ['class' => 'form-control' ]) !!}
                        </div>
                        {{--<div class="form-group">
                            {!! Form::label('responsabilidad', 'Responsabilidad' ) !!}
                            {!! Form::select('responsabilidad', ['1' => 'Responsabilidad1', '2' => 'Responsabilidad2'], null, ['class' => 'form-control']) !!}
                            {!! Form::text('responsabilidad', null, ['class' => 'form-control' ]) !!}
                        </div> --}}
                        <div class="form-group">
                            {!! Form::label('cemail', 'Correo Electronico' ) !!}
                            {!! Form::email('cemail', null, ['class' => 'form-control' ]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ctelempleado', 'Telefono' ) !!}
                            {!! Form::text('ctelempleado', null, ['class' => 'form-control' ]) !!}
                        </div>
                        @include('layouts.errors')
                        {!! Form::submit('Registrar', ['class' => 'btn btn-primary' ]) !!}
                        {{-- {!! Form::submit('Registrar', ['class' => 'btn btn-primary' ]) !!} --}}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
