@extends('layouts.app')

@section('title', 'Nuevo Productor')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-3"><a class="btn btn-primary" href="/home">Salir</a></div>
                        <div class="col-md-9"><h2>Modificar Productor</h2></div>
                    </div>
                </div>
                <div class="card-body">

                    {!! Form::open(['url' => 'Productor/'.$productores->id, 'method' => 'PATCH'] ) !!}
                    {{-- {!! Form::model($productores, ['route' => 'Nuevo_Productor.update', $productores], 'method' => 'PUT') !!} --}}
                        <div class="form-group">
                            {!! Form::label('cnombproductor', 'Nombre' ) !!}
                            {!! Form::text('cnombproductor', $productores->cnombproductor, ['class' => 'form-control' ])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('capeproductor', 'Apellido' ) !!}
                            {!! Form::text('capeproductor', $productores->capeproductor, ['class' => 'form-control' ]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ncedula', 'Cedula' ) !!}
                            {!! Form::text('ncedula', $productores->ncedula, ['class' => 'form-control' ]) !!}
                        </div>
                        {{-- <div class="form-group"> --}}
                            {{-- {!! Form::label('responsabilidad', 'Responsabilidad' ) !!} --}}
                            {{-- {!! Form::select('responsabilidad', ['1' => 'Responsabilidad1', '2' => 'Responsabilidad2'], null, ['class' => 'form-control']) !!} --}}
                            {{-- {!! Form::text('responsabilidad', null, ['class' => 'form-control' ]) !!} --}}
                        {{-- </div> --}}
                        <div class="form-group">
                            {!! Form::label('cemail', 'Correo Electronico' ) !!}
                            {!! Form::email('cemail', $productores->cemail, ['class' => 'form-control' ]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ctelf', 'Telefono' ) !!}
                            {!! Form::text('ctelf', $productores->ctelf, ['class' => 'form-control' ]) !!}
                        </div>
                        {{-- <div class="form-group"> --}}
                           {{--  {!! Form::label('proyecto', 'Proyecto' ) !!}
                            {!! Form::select('proyecto', ['1' => 'Proyecto1', '2' => 'Proyecto2'], ['class' => 'form-control box-size', 'id'=>'id_proyecto']); !!} --}}
                        {{-- </div> --}}
                        <div class="form-group">
                                {!! Form::label('tb_identpryts_id', 'Asignar Proyecto' ) !!}
                                {!! Form::select('tb_identpryts_id', $proyectos->pluck('nombre_pry', 'id'), isset($proyectos) ? $proyectos : null, ['class' => 'form-control box-size', 'id'=>'id']); !!}
                            </div>
                        @include('layouts.errors')
                        {!! Form::submit('Actualizar', ['class' => 'btn btn-primary' ]) !!}
                        {{-- {!! Form::submit('Registrar', ['class' => 'btn btn-primary' ]) !!} --}}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
