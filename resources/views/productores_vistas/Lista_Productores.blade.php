@extends('layouts.app')

@section('title', 'Nuevo Productor')

@section('content')
<div class="container">
     <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    @include('layouts.success')
                    <div class="row">
                        <div class="col-md-3"><a class="btn btn-primary" href="/home">Salir</a></div>
                        <div class="col-md-6"><h1>Listado de Productores</h1></div>
                        <div class="col-md-3"><a href="/Productor/create" class="btn btn-primary">Registrar Productor</a></div>
                    </div>
                </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                    <thead>
                                            <!-- se coloco h4 por prueba-->
                                        <th class="info"><h4>Cedula</h4></th>
                                        <th class="info"><h4>Nombre y Apellido</h4></th>
                                        <th class="info"><h4>Email</h4></th>
                                        <th class="info"><h4>Telefono</h4></th>
                                        <th class="info"><h4>Proyecto</h4></th>
                                        <th class="info"><h4>Accion</h4></th>
                                    </thead>
                                <body>
                                 @foreach ($consulta as $consult)
                                    <tr>
                                        {{-- @if ($consult->id != 1) --}} {{-- activar para que la consulta funcione con el id 1 --}}
                                            <td>{{ $consult-> ncedula }}</td>
                                            <td>{{ $consult-> cnombproductor }} {{ $consult-> capeproductor }}</td>
                                            <td>{{ $consult-> cemail }}</td>
                                            <td>{{ $consult-> ctelf }}</td>
                                            @foreach ($consult->tb_identpryt as $value)
                                                {{-- @if (empty(!$value->id)) --}}
                                                    <td>{{ $value->nombre_pry }}</td>
                                                {{-- @else --}}
                                                    {{-- <td>Sin Proyecto Asignado</td> --}}
                                                {{-- @endif --}}
                                            @endforeach
                                            <td>
                                                <a href="/Productor/{{ $consult-> id }}/edit" class="btn btn-warning btn-sm">Editar</a>
                                            </td>
                                    </tr>
                                @endforeach
                                </body>
                            </table>
                        </div>
                    {{-- {{ $consulta->links() }} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection