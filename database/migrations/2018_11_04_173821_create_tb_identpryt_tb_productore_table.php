<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbIdentprytTbProductoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_identpryt_tb_productore', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tb_identpryt_id')->unsigned();
            $table->integer('tb_productore_id')->unsigned();
            $table->timestamps();

            $table->foreign('tb_identpryt_id')->references('id')->on('tb_identpryts');
            $table->foreign('tb_productore_id')->references('id')->on('tb_productores');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_identpryt_tb_productore');
    }
}
