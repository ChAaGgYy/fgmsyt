<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbFigJuridsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_fig_jurids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 200);
            $table->timestamps();
        });

        $file = realpath(__DIR__.'/../organizacion/tb_fig_jurid.sql');

        DB::unprepared( file_get_contents($file) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_fig_jurids');
    }
}
