<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbIdentprytsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_identpryts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_pry', 50);
            $table->string('nombre_pry', 200);
            $table->string('descrip_pry', 200)nullable();
            $table->string('act_econom_pry', 200);
            $table->boolean('registrado');
            $table->integer('id_letra_rif')->unsigned(); //id de otra tabla
            $table->integer('id_ciudad')->unsigned(); //id de otra tabla
            $table->integer('id_estado')->unsigned(); //id de otra tabla
            $table->integer('id_municipio')->unsigned(); //id de otra tabla
            $table->integer('id_parroquia')->unsigned(); //id de otra tabla
            $table->integer('numer_rif')->unsigned();
            $table->integer('numer_ident_rif_pry')->unsigned();
            $table->string('codigo_situr', 21)nullable();
            $table->string('codigo_sunagro', 10)nullable();
            $table->string('direccion', 250);
            $table->string('telefono', 250);
            $table->date('fech_ini');
            $table->timestamps();
            $table->foreign('id_letra_rif')->references('id')->on('tb_letras_rifs');
            $table->foreign('id_ciudad')->references('id')->on('tb_ciudades');
            $table->foreign('id_municipio')->references('id')->on('tb_municipios');
            $table->foreign('id_estado')->references('id')->on('tb_estados');
            $table->foreign('id_parroquia')->references('id')->on('tb_parroquias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_identpryts');
    }
}
