<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMunicipiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_municipios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_estado');
            $table->string('municipio', 100);
            $table->timestamps();
            $table->foreign('id_estado')->references('id')->on('tb_estados');
        });

        $file = realpath(__DIR__.'/../organizacion/tb_municipios.sql');

        DB::unprepared( file_get_contents($file) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_municipios');
    }
}
