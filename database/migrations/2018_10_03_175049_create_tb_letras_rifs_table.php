<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbLetrasRifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_letras_rifs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('letra', 1);
            $table->string('descripcion');
            $table->timestamps();
        });

        $file = realpath(__DIR__.'/../organizacion/tb_letras_rif.sql');

        DB::unprepared( file_get_contents($file) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_letras_rifs');
    }
}
