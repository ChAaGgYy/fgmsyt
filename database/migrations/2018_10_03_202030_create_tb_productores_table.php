<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbProductoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_productores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cnombproductor', 50); //nombre del productor
            $table->string('capeproductor', 50); //apellido del productor
            $table->integer('ncedula')->unique(); //cedula del productor
            $table->string('cemail'); // correo electronico del productor
            $table->string('ctelf')->nullable(); //telefono del productor
            // $table->integer('tb_identpryts_id')->nullable();
            $table->timestamps();
            // $table->foreign('tb_identpryts_id')->references('id')->on('tb_identpryts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_productores');
    }
}
