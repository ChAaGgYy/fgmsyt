<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbParroquiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_parroquias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_municipio');
            $table->string('parroquia', 250);
            $table->timestamps();
            $table->foreign('id_municipio')->references('id')->on('tb_municipios');
        });

        $file = realpath(__DIR__.'/../organizacion/tb_parroquias.sql');

        DB::unprepared( file_get_contents($file) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_parroquias');
    }
}
