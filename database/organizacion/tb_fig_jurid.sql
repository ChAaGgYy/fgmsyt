INSERT INTO tb_fig_jurids (id, descripcion) VALUES
(1,'Empresa de Propiedad Social Directa Comuna'),
(2,'Grupo de Intercambio Solidario y Trueque'),
(3,'Unidad Productiva Familiar'),
(4,'Asociación Cooperativa'),
(5,'Empresa de Propiedad Social Indirecta Comunal'),
(6,'UPF - Unidad Productiva Familiar'),
(7,'Firma Personal'),
(8,'C.A. - Compañía Anónima'),
(9,'S.A. - Sociedad Anónima'),
(10,'S.R.L. - Sociedad de Responsabilidad Limitada');