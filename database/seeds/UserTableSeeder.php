<?php

use Illuminate\Database\Seeder;
use fgmsyt\User;
use fgmsyt\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_user = Role::where('name', 'Usuario')->first();
		$role_admin = Role::where('name', 'Administrador')->first();

		$user = new User();
		$user->name = 'User';
		$user->email = 'user@example.com';
		$user->password = bcrypt('secret');
		$user->save();
		$user->roles()->attach($role_user);
		
		$user = new User();
		$user->name = 'Admin';
		$user->email = 'admin@example.com';
		$user->password = bcrypt('secret');
		$user->save();
		$user->roles()->attach($role_admin);
    }
}
