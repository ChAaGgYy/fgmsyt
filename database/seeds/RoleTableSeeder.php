<?php

use Illuminate\Database\Seeder;
use fgmsyt\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = new Role();
		$role->name = 'Administrador';
		$role->description = 'Administrator del sistema';
		$role->save();

		$role = new Role();
		$role->name = 'Usuario';
		$role->description = 'Ususario del sistema';
		$role->save();
    }
}
