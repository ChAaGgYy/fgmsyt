<?php

namespace fgmsyt;

use Illuminate\Database\Eloquent\Model;

class tb_estado extends Model
{
    //
    protected $fillable = ['estado'];
}
