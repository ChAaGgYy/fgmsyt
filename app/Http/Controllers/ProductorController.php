<?php

namespace fgmsyt\Http\Controllers;

use Illuminate\Http\Request;
use fgmsyt\tb_productore;
use fgmsyt\tb_identpryt;
use DB;

class ProductorController extends Controller
{

        public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        // $request->user()->authorizeRoles('Administrador');
        // if($request->ajax()){
        //     return response()->json([
        //         ['id' => 1, 'name' => 'nombre desde el controaldor']
        //     ]);
        // }

        // return view('productores_vistas.index');


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $productores = tb_productore::all();
        // dd($productores);
        $idptrys = 1;
        return view('productores_vistas.Nuevo_Productor', compact('productores', 'idptrys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd(request()->all());
        $rules = [
            'cnombproductor' => 'required',
            'capeproductor' => 'required',
            'ncedula' => 'required|unique:tb_productores|max:8',
            'cemail'  => 'required',
            'ctelempleado' => 'required'
        ];

        $messages = [
            'cnombproductor.required' => 'El Nombre es Obligatorio',
            'capeproductor.required' => 'El  Apellido es Obligatorio',
            'ncedula.required' => 'La Cedula es Obligatoria',
            'ncedula.unique' => 'La Cedula ya se encuentra Registrado.',
            'cemail.required' => 'El Correlo Electronico es Obligatorio',
            'ctelempleado.required' => 'El Telefono es Obligatorio',
        ];
        $this->validate($request, $rules, $messages);

        tb_productore::create($request->all());

        return redirect('Lista_Productores');  //para redirigirlo a la lista de productores
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // return $productores;
         $productores = tb_productore::findOrFail($id);
         // dd($productores);
         $proyectos = tb_identpryt::all();

         // dd($proyectos);

            return view('productores_vistas.Edit_Productor', compact('productores', 'proyectos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // tb_productore::where('id', $id)->update($request->all());

        tb_productore::where('id', $id)->update($request->except(['_method','_token']));

        $prod = tb_productore::find($id);
        $prod->tb_identpryt()->attach($prod->tb_identpryts_id);
        // dd($prod);



        return redirect('Lista_Productores')->with('status', 'Actualización Satisfactoria');
    }

    public function asignarProyecto(Request $request, $id)
    {
        //
        // tb_productore::where('id', $id)->update($request->all());

        tb_productore::where('id', $id)->update($request->except(['_method','_token']));

        $prod = tb_productore::find($id);
        $prod->tb_identpryt()->attach($prod->tb_identpryts_id);
        // dd($prod);



        return redirect('Lista_Productores')->with('status', 'Actualización Satisfactoria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function consulta()
    {
        //$consulta = Empleado::all();
        // $productores = tb_productore::all();
        // $consulta = tb_productore::with(['tb_identpryt'])->get();
        // $consulta = DB::table('tb_productores')
        //             ->join('tb_identpryt_tb_productore', 'tb_identpryt_tb_productore.id', '=', 'tb_productores.id')
        //             // ->join('tb_identpryt_tb_productore', 'tb_identpryt_tb_productore.id', '=', 'tb_identpryts.id')
        //             // -> select('tb_productores.id','tb_productores.ncedula', 'tb_productores.cnombproductor', 'tb_productores.capeproductor', 'tb_productores.cemail', 'tb_productores.ctelempleado', 'tb_identpryts.nombre_pry')
        //             ->get();
        // $consulta = tb_productore::orderBy('ncedula', 'ASC')->paginate(10);
        // $proy = $consulta->tb_identpryt->nombre_pry;
        $consulta = tb_productore::all();
        //dd($consulta);

        return view('productores_vistas.Lista_Productores', compact('consulta'));
    }
}
