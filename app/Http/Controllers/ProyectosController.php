<?php

namespace fgmsyt\Http\Controllers;

use Illuminate\Http\Request;
use fgmsyt\tb_letras_rif;
use fgmsyt\tb_identpryt;
use fgmsyt\tb_ciudade;
use fgmsyt\tb_estado;
use fgmsyt\tb_municipio;
use fgmsyt\tb_parroquia;

class ProyectosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return 'Metodo index de ProyectosController';
        // return view('proyectos_vistas.Nuevo_Proyecto');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $letras = tb_letras_rif::all();
        $ciudades = tb_ciudade::all();
        $estados = tb_estado::all();
        $municipios = tb_municipio::all();
        $parroquias = tb_parroquia::all();
        // dd($estados);
        return view('proyectos_vistas.Nuevo_Proyecto', compact('letras', 'ciudades', 'estados' , 'municipios', 'parroquias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd(request()->all());

        $rules = [
            'numero_pry' => 'required',
            'nombre_pry' => 'required',
            'numer_ident_rif_pry' => 'required',
            'numer_rif' => 'required',
            'act_econom_pry' => 'required',
            'telefono'  => 'required',
            'direccion' => 'required',
            'fech_ini' => 'required'
        ];

        $messages = [
            'numero_pry.required' => 'El Número del Proyecto Obligatorio',
            'nombre_pry.required' => 'El Nombre del Proyecto es Obligatorio',
            'numer_ident_rif_pry.required' => 'El Número de Chequeo del Rif es Obligatorio',
            'numer_rif.required' => 'El Número del Rif es Obligatorio',
            'act_econom_pry.required' => 'Debe Indicar una Aactividad Económica',
            'telefono.required' => 'El Telefono es Obligatoria',
            'direccion.required' => 'Debe Especificar una Dirección',
            'fech_ini.required' => 'Debe indicar una Fecha del Proyecto',
        ];
        $this->validate($request, $rules, $messages);


        tb_identpryt::create(request()->all());


        return redirect('Lista_Proyectos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getmunicipios($id)
    {
        // $a = response()->json(tb_municipio::where('id_estado', $id)->get());
        // dd($a);
        return response()->json(tb_municipio::where('id_estado', $id)->get());
    }

    public function consulta()
    {
        $proyectos = tb_identpryt::all();
        $consulta = tb_identpryt::orderBy('numero_pry', 'ASC')->paginate(10);
        // dd($consulta);
        return view('proyectos_vistas.Lista_Proyectos', compact('consulta'));
    }
}
