<?php

namespace fgmsyt;

use Illuminate\Database\Eloquent\Model;

class tb_ciudade extends Model
{
    //
    protected $fillable = ['ciudad'];
}
