<?php

namespace fgmsyt;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //

    public function users()
	{
		return $this->belongsToMany('fgmsyt\User')->withTimestamps();
	}
}
