<?php

namespace fgmsyt;

use Illuminate\Database\Eloquent\Model;

class tb_productore extends Model
{
    //
    protected $fillable = ['cnombproductor', 'capeproductor', 'ncedula', 'cemail', 'ctelf'];



    public function tb_identpryt()
	{
	   return $this->belongsToMany(tb_identpryt::class)->withTimestamps();
	}


}


