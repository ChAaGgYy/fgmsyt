<?php

namespace fgmsyt;

use Illuminate\Database\Eloquent\Model;

class tb_parroquia extends Model
{
    //
    protected $fillable = ['parroquia'];
}
