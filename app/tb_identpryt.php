<?php

namespace fgmsyt;

use Illuminate\Database\Eloquent\Model;

class tb_identpryt extends Model
{
    //

    protected $fillable = ['numero_pry','nombre_pry','descrip_pry', 'act_econom_pry','registrado', 'id_letra_rif', 'numer_rif', 'id_estado', 'id_ciudad', 'id_municipio', 'id_parroquia', 'numer_ident_rif_pry', 'codigo_situr', 'codigo_sunagro', 'direccion', 'telefono','fech_ini'];


 //    public function productore()
	// {
	//    return $this->hasMany('fgmsyt\tb_productore', 'id_identpryts', 'id');
	// }

	// public function tb_productore()
	// {
	//    return $this->hasMany(tb_productore::class, 'tb_identpryt_id');
	// }

	public function tb_productore()
	{
	   return $this->belongsToMany(tb_productore::class)->withTimestamps();
	}


}
