<?php

namespace fgmsyt;

use Illuminate\Database\Eloquent\Model;

class tb_municipio extends Model
{
    //
    protected $fillable = ['municipio'];
}
